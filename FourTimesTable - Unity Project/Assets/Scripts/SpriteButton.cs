﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class SpriteButton : MonoBehaviour, IPointerDownHandler
{
    public QuestionController questionController;
    public IsometricController isometricController;
    public TMP_Text butText;
    public bool isCorrectAns;
    private string buttonName;
    public string buttonDirection;
    
    // Start is called before the first frame update
    void Start()
    {
        questionController = GameObject.Find("GameControl").gameObject.GetComponent<QuestionController>();
        isometricController = GameObject.Find("Player Bus").gameObject.GetComponent<IsometricController>();

        buttonName = gameObject.name;
        switch(buttonName)
        {
            case "UpSquare":
                buttonDirection = "N";
            break;

            case "RightSquare":
                buttonDirection = "E";
            break;

            case "DownSquare":
                buttonDirection = "S";
            break;

            case "LeftSquare":
                buttonDirection = "W";
            break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerDown(PointerEventData pointerEventData)
    {
        if(isCorrectAns)
        {
            questionController.CorrectAnswer(buttonDirection);
        }
        else
        {
            isometricController.SpawnWrongTile(buttonDirection);
            gameObject.SetActive(false);
        }
    }
}
