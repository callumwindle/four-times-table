﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteController : MonoBehaviour
{
    public Sprite directionalSprite;
    public bool isAnimated;
    public int busAnimation;
}
