﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionController : MonoBehaviour
{
    public bool isCorrect;
    public int curQues, qAns, randomNum, randomAns, randomButton;
    public List<GameObject> usableButtons;
    public List<int> questionList, possibleNumbers;
    public IsometricController isometricController;
    public GameObject buttonGroup, qSign, qText;

    private SpriteButton sbScript;
    
    // Start is called before the first frame update
    void Start()
    {
        questionList = new List<int>();
        for(int x=1; x<= 12; x++)
        {
            questionList.Add(x);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NewQuestion()
    {
        qSign.SetActive(true);
        
        possibleNumbers = new List<int>();

        for(int a=1; a<51; a++)
        {
            if(a%4 != 0)
            {
                possibleNumbers.Add(a);
            }
        }
        
        randomNum = Random.Range(0,questionList.Count);
        curQues = questionList[randomNum];
        questionList.RemoveAt(randomNum);
        qAns = 4 * curQues;
        qText.GetComponent<Text>().text = "4x"+curQues.ToString()+"=?";
        NewAnswers();
    }

    public void NewAnswers()
    {
        buttonGroup = isometricController.spawnedTile.gameObject.transform.Find("Buttons").gameObject;
        usableButtons = new List<GameObject>();

        for(int i=0; i < buttonGroup.transform.childCount; i++)
        {
            usableButtons.Add(buttonGroup.transform.GetChild(i).gameObject);
        }

        randomButton = Random.Range(0, usableButtons.Count);

        for(int x=0; x<usableButtons.Count; x++)
        {
            sbScript = usableButtons[x].GetComponent<SpriteButton>();
            
            if(x == randomButton)
            {
                sbScript.isCorrectAns = true;
                sbScript.butText.text = qAns.ToString();
            }
            else
            {
                sbScript.isCorrectAns = false;
                randomNum = Random.Range(0, possibleNumbers.Count);
                sbScript.butText.text = possibleNumbers[randomNum].ToString();
                possibleNumbers.Remove(randomNum);
            }
        }
    }
    
    public void CorrectAnswer(string button)
    {
        foreach(GameObject i in usableButtons)
        {
            i.SetActive(false);
        }
        
        switch(button)
        {
            case "N":
                isometricController.SpawnNewTile("N");
            break;

            case "E":
                isometricController.SpawnNewTile("E");
            break;

            case "S":
                isometricController.SpawnNewTile("S");
            break;

            case "W":
                isometricController.SpawnNewTile("W");
            break;
        }
    }
}
