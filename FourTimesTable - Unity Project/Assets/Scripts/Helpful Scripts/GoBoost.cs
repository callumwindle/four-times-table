﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GoBoost : EditorWindow
{
    // Start is called before the first frame update

    
    private string nameString;

    [MenuItem("Go Tools/Boost Required")]
    public static void ShowWindow()
    {
        GetWindow(typeof(GoBoost));
    }

    private void OnGUI() {
        EditorGUILayout.LabelField("Unity stressing you out? Need that extra boost to get through the day?",EditorStyles.wordWrappedLabel);
        EditorGUILayout.Space();
        nameString = EditorGUILayout.TextField("Your Name:  ", nameString);
        if(GUILayout.Button("Boost Required")){
            int temp = Random.Range(0,100);
            if(temp == 69){
            Debug.Log("Don't tell anyone " + nameString +  " but Danny said he doesn't like your face... Do with that what you will...");
            }
            else{
            Debug.Log("Well Done! " + nameString +  " this game is looking pretty good, proud of you. Chris");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
